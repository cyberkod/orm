<?php

/*
CREATE TABLE `__User` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupID` int(10) unsigned DEFAULT NULL,
  `deleted` tinyint(3) unsigned NOT NULL,
  `hidden` tinyint(3) unsigned NOT NULL,
  `root` tinyint(3) unsigned NOT NULL,
  `forceChange` tinyint(3) unsigned NOT NULL COMMENT 'force change password',
  `modifyDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creationTime` time NOT NULL DEFAULT '00:00:00',
  `password` char(40) NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`userID`),
  KEY `GroupID` (`groupID`),
  KEY `Email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
 */

/**
 * @engine InnoDB
 * @charset utf8
 * @collate utf8_polish_ci
 */
class User extends Cyberkod\ORM\Entity
{
    /**
     * @type int(10) unsigned
     * @auto_increment
     * @primary
     */
    public $userID;

    /**
     * @type int(10) unsigned
     * @null true
     * @key
     */
    public $groupID = null;

    /**
     * @type tinyint(3) unsigned
     */
    public $deleted;

    /**
     * @type tinyint(3) unsigned
     */
    public $hidden;

    /**
     * @type tinyint(3) unsigned
     */
    public $root;

    /**
     * @type tinyint(3) unsigned
     * @comment "force change password"
     */
    public $forceChange;

    /**
     * @type datetime
     */
    public $modifyDateTime = '0000-00-00 00:00:00';

    /**
     * @type time
     */
    public $creationTime = '00:00:00';

    /**
     * @type char(40)
     */
    public $password = '';

    /**
     * @type tinytext
     * @collate utf8_general_ci
     * @fulltext
     */
    public $name = '';

    /**
     * @type varchar(128)
     * @unique unique_email_key
     */
    public $email = '';

    public $normalField;
}