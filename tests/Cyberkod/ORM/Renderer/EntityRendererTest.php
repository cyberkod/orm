<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Renderer\EntityRenderer;
use Cyberkod\ORM\Parser\Annotation\EntityParser as AnnotationEntityParser;

require_once (__DIR__.'/../../../resources/entity/User.php');

class EntityRendererTest extends \PHPUnit_Framework_TestCase
{
    public static $userRenderer;

    public static function setUpBeforeClass()
    {
        $reflection = new \ReflectionClass(\User::class);
        $parser = new AnnotationEntityParser($reflection);
        self::$userRenderer = new EntityRenderer($parser);
    }

    function testRender()
    {
        
    }
}