<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\Field\Date as FieldDate;

class DateTest extends \PHPUnit_Framework_TestCase
{
    public function testDefault()
    {
        $field = FieldDate::create('foo', 'date');
        $this->assertInstanceOf(FieldDate::class, $field);
        $this->assertSame('date', $field->type);
    }

    public function testAllowedTypes()
    {
        FieldDate::create('foo', 'datetime');
        FieldDate::create('foo', 'datetime2');
        FieldDate::create('foo', 'smalldatetime');
        FieldDate::create('foo', 'date');
        FieldDate::create('foo', 'time');
        FieldDate::create('foo', 'datetimeoffset');
        FieldDate::create('foo', 'timestamp');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidType()
    {
        FieldDate::create('foo', 'datetime(255)');
    }
}