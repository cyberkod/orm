<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\Field\DynamicStr as FieldDynamicStr;

class DynamicStrTest extends \PHPUnit_Framework_TestCase
{

    public function testChar()
    {
        $field = FieldDynamicStr::create('foo', 'char(24)');
        $this->assertInstanceOf(FieldDynamicStr::class, $field);
        $this->assertSame('char', $field->type);
        $this->assertSame(24, $field->length);
    }

    public function testLength()
    {
        $field = FieldDynamicStr::create('foo', 'varchar(32)');
        $this->assertInstanceOf(FieldDynamicStr::class, $field);
        $this->assertSame('varchar', $field->type);
        $this->assertSame(32, $field->length);
    }

    public function testAllowedTypes()
    {
        FieldDynamicStr::create('foo', 'char(64)');
        FieldDynamicStr::create('foo', 'varchar(64)');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidLength()
    {
        FieldDynamicStr::create('foo', 'varchar(-4) unsigned');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidType()
    {
        FieldDynamicStr::create('foo', 'char');
    }
}