<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\Field\Integer as FieldInteger;

class IntegerTest extends \PHPUnit_Framework_TestCase
{

    public function testDefault()
    {
        $field = FieldInteger::create('foo', 'int');
        $this->assertInstanceOf(FieldInteger::class, $field);
        $this->assertSame('int', $field->type);
        $this->assertSame(0, $field->length);
        $this->assertSame(false, $field->unsigned);
    }

    public function testUppercase()
    {
        $field = FieldInteger::create('foo', 'TINYINT');
        $this->assertInstanceOf(FieldInteger::class, $field);
        $this->assertSame('tinyint', $field->type);
    }

    public function testUnsigned()
    {
        $field = FieldInteger::create('foo', 'int unsigned');
        $this->assertInstanceOf(FieldInteger::class, $field);
        $this->assertSame('int', $field->type);
        $this->assertSame(0, $field->length);
        $this->assertSame(true, $field->unsigned);
    }

    public function testLength()
    {
        $field = FieldInteger::create('foo', 'bigint(4)');
        $this->assertInstanceOf(FieldInteger::class, $field);
        $this->assertSame('bigint', $field->type);
        $this->assertSame(4, $field->length);
        $this->assertSame(false, $field->unsigned);
    }

    public function testLengthAndUnsigned()
    {
        $field = FieldInteger::create('foo', 'smallint(11) unsigned');
        $this->assertInstanceOf(FieldInteger::class, $field);
        $this->assertSame('smallint', $field->type);
        $this->assertSame(11, $field->length);
        $this->assertSame(true, $field->unsigned);
    }

    public function testAllowedTypes()
    {
        FieldInteger::create('foo', 'tinyint');
        FieldInteger::create('foo', 'smallint');
        FieldInteger::create('foo', 'mediumint');
        FieldInteger::create('foo', 'int');
        FieldInteger::create('foo', 'bigint');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidLength()
    {
        FieldInteger::create('foo', 'tinyint(-4) unsigned');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidType()
    {
        FieldInteger::create('foo', 'varchar(255)');
    }
}