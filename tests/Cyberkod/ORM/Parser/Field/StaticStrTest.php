<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\Field\StaticStr as FieldStaticStr;

class StaticStrTest extends \PHPUnit_Framework_TestCase
{
    public function testDefault()
    {
        $field = FieldStaticStr::create('foo', 'tinytext');
        $this->assertInstanceOf(FieldStaticStr::class, $field);
        $this->assertSame('tinytext', $field->type);
        $this->assertSame(0, $field->length);
    }

    public function testAllowedTypes()
    {
        FieldStaticStr::create('foo', 'tinytext');
        FieldStaticStr::create('foo', 'text');
        FieldStaticStr::create('foo', 'mediumtext');
        FieldStaticStr::create('foo', 'longtext');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidLength()
    {
        FieldStaticStr::create('foo', 'mediumtext(-4) unsigned');
    }

    /**
     * @expectedException Cyberkod\ORM\Exception\InvalidFieldType
     */
    public function testValidType()
    {
        FieldStaticStr::create('foo', 'tinytext(255)');
    }
}