<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\FieldFactory;
use Cyberkod\ORM\Parser\Field;
use Cyberkod\ORM\Parser\Field\Integer as FieldInteger;

class FieldFactoryTest extends \PHPUnit_Framework_TestCase
{
    public static $factory;

    public static function setUpBeforeClass()
    {
        self::$factory = new FieldFactory();
    }

    public function assertInstanceOfField($expected, $actual)
    {
        $this->assertInstanceOf($expected, $actual);
        $this->assertInstanceOf(Field::class, $actual);
    }

    public function testFieldInteger()
    {
        $field = self::$factory->create('foobar', 'int');
        $this->assertInstanceOfField(FieldInteger::class, $field);
        $field = self::$factory->create('foobar', 'int(11)');
        $this->assertInstanceOfField(FieldInteger::class, $field);
        $field = self::$factory->create('foobar', 'smallint(5) unsigned');
        $this->assertInstanceOfField(FieldInteger::class, $field);
    }
}