<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\Table;

class TableTest extends \PHPUnit_Framework_TestCase
{
    function testCreationTable()
    {
        $table = new Table('foobar');
        $this->assertSame('foobar', $table->name);
    }
}