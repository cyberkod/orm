<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Parser\Key;
use Cyberkod\ORM\Parser\Annotation\EntityParser as AnnotationEntityParser;

require_once (__DIR__.'/../../../../resources/entity/User.php');

class EntityParserTest extends \PHPUnit_Framework_TestCase
{
    public static $userTable;

    public static function setUpBeforeClass()
    {
        $reflection = new \ReflectionClass(\User::class);
        $parser = new AnnotationEntityParser($reflection);
        self::$userTable = $parser->getTable();
    }

    public function testTableUser()
    {

        $this->assertSame('user', self::$userTable->name);
        $this->assertSame('InnoDB', self::$userTable->engine);
        $this->assertSame('utf8', self::$userTable->charset);
        $this->assertSame('utf8_polish_ci', self::$userTable->collate);
    }

    public function testTableStdClass()
    {
        $reflection = new \ReflectionClass(\stdClass::class);
        $parser = new AnnotationEntityParser($reflection);
        $table = $parser->getTable();

        $this->assertSame('stdclass', $table->name);
        $this->assertSame('MyISAM', $table->engine);
        $this->assertSame('utf8', $table->charset);
        $this->assertSame('utf8_general_ci', $table->collate);
    }

    public function testTableUserFields()
    {
        $this->assertCount(11, self::$userTable->getFields());
    }

    public function testTableUserKeys()
    {
        $this->assertCount(4, self::$userTable->getKeys());
    }

    /**
     * @depends testTableUserFields
     */
    public function testUserIDField()
    {
        $field = self::$userTable->getFieldByName('userID');

        $this->assertSame('userID', $field->name);
        $this->assertSame('int', $field->type);
        $this->assertSame(10, $field->length);
        $this->assertSame('', $field->comment);
        $this->assertTrue($field->unsigned);
        //$this->assertSame('`userID` int(10) unsigned NOT NULL AUTO_INCREMENT', $field->getCreationSyntax());
    }

    public function testCreateSyntax()
    {
        $creationSyntaxes = array(
            "`userID` int(10) unsigned NOT NULL AUTO_INCREMENT",
            "`groupID` int(10) unsigned DEFAULT NULL",
            "`deleted` tinyint(3) unsigned NOT NULL",
            "`hidden` tinyint(3) unsigned NOT NULL",
            "`root` tinyint(3) unsigned NOT NULL",
            "`forceChange` tinyint(3) unsigned NOT NULL COMMENT 'force change password'",
            "`modifyDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'",
            "`creationTime` time NOT NULL DEFAULT '00:00:00'",
            "`password` char(40) NOT NULL DEFAULT ''",
            "`name` tinytext NOT NULL DEFAULT ''",
            "`email` varchar(128) NOT NULL DEFAULT ''",
        );

        foreach (self::$userTable->getFields() as $field) {
            $syntax = array_shift($creationSyntaxes);
            $this->assertSame($syntax, $field->getCreationSyntax());
        }
    }

    public function testPrimaryKey()
    {
        $field = self::$userTable->getFieldByName('userID');
        $key = $field->getKey();

        $this->assertSame('PRIMARY', $key->getName());
        $this->assertInstanceOf(Key\Primary::class, $key);
        $this->assertSame(
            'PRIMARY KEY (`userID`)', $key->getCreationSyntax()
        );
    }

    public function testUniqueKey()
    {
        $field = self::$userTable->getFieldByName('email');
        $key = $field->getKey();

        $this->assertSame('unique_email_key', $key->getName());
        $this->assertInstanceOf(Key\Unique::class, $key);
        $this->assertSame(
            'UNIQUE KEY `unique_email_key` (`email`)', $key->getCreationSyntax()
        );
    }

    public function testIndexKey()
    {
        $field = self::$userTable->getFieldByName('groupID');
        $key = $field->getKey();

        $this->assertSame('groupid', $key->getName());
        $this->assertInstanceOf(Key\Index::class, $key);
        $this->assertSame(
            'KEY `groupid` (`groupID`)', $key->getCreationSyntax()
        );
    }

    public function testFullTextKey()
    {
        $field = self::$userTable->getFieldByName('name');
        $key = $field->getKey();

        $this->assertSame('name', $key->getName());
        $this->assertInstanceOf(Key\FullText::class, $key);
        $this->assertSame(
            'FULLTEXT KEY `name` (`name`)', $key->getCreationSyntax()
        );
    }

    public function testCreateSyntaxFullQuery()
    {
        $creationSyntaxes = array(
            "CREATE TABLE `user` (",
            "`userID` int(10) unsigned NOT NULL AUTO_INCREMENT,",
            "`groupID` int(10) unsigned DEFAULT NULL,",
            "`deleted` tinyint(3) unsigned NOT NULL,",
            "`hidden` tinyint(3) unsigned NOT NULL,",
            "`root` tinyint(3) unsigned NOT NULL,",
            "`forceChange` tinyint(3) unsigned NOT NULL COMMENT 'force change password',",
            "`modifyDateTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',",
            "`creationTime` time NOT NULL DEFAULT '00:00:00',",
            "`password` char(40) NOT NULL DEFAULT '',",
            "`name` tinytext NOT NULL DEFAULT '',",
            "`email` varchar(128) NOT NULL DEFAULT '',",
            "PRIMARY KEY (`userID`),",
            "KEY `groupid` (`groupID`),",
            "FULLTEXT KEY `name` (`name`),",
            "UNIQUE KEY `unique_email_key` (`email`)",
            ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;",
        );

        $createSyntax = self::$userTable->getCreationSyntax();
        $splited = preg_split('/\R/m', $createSyntax);

        foreach ($splited as $part) {
            $syntax = array_shift($creationSyntaxes);
            $this->assertSame($syntax, trim($part));
        }
    }
}