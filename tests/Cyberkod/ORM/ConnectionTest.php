<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Connection;

class ConnectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @group database
     */
    public function testCreateMulti()
    {
        new Connection(
            $GLOBALS['DB1_DSN'],
            $GLOBALS['DB1_USER'],
            $GLOBALS['DB1_PASSWD']
        );
    }
}