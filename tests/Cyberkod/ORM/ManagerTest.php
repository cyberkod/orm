<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Manager;
use Cyberkod\ORM\Entity;
use Cyberkod\ORM\Test\ConnectionDatabaseTestCase;

require_once (__DIR__.'/../../resources/entity/User.php');
require_once (__DIR__.'/../../ConnectionDatabaseTestCase.php');

class ManagerTest extends ConnectionDatabaseTestCase
{
    public static $manager;

    public static function setUpBeforeClass()
    {
        self::$manager = new Manager(self::getConnection());
        self::$manager->registerEntity('user', \User::class);
    }

    public function testCreateUserEntity()
    {
        $user = self::$manager->createEntity('user');
        $this->assertInstanceOf(\User::class, $user);
        $this->assertInstanceOf(Entity::class, $user);
        $this->assertSame('0000-00-00 00:00:00', $user->modifyDateTime);
        $this->assertSame('00:00:00', $user->creationTime);
        $this->assertSame(0, $user->getPrimary());
        $this->assertFalse($user->isPreserve());
    }

    /**
     * @group database
     */
    public function testGetUserEntity()
    {
        $userEntity = self::$manager->createEntity('user');
        $userEntity->name = 'somename';
        $userEntity->save();
        $id = $userEntity->getPrimary();
        /*$this->assertNotEquals(0, $id);

        $userSelector = self::$manager->createSelector('user');
        $userEntityBySelector = $userSelector->getByPrimaryID($id);

        $this->assertEquals($userEntity, $userEntityBySelector);*/
    }
}