<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\EntityManager;

require_once (__DIR__.'/../../resources/entity/User.php');

class EntityManagerTest extends \PHPUnit_Framework_TestCase
{
    public static $entityManager;

    public static function setUpBeforeClass()
    {
        self::$entityManager = new EntityManager();
        self::$entityManager->addEntityClass('user', User::class);
    }

    public function testCreation()
    {
        self::$entityManager->create('user');
    }
}