<?php

namespace Cyberkod\ORM\Test;

use Cyberkod\ORM\Connection;

abstract class ConnectionDatabaseTestCase extends \PHPUnit_Framework_TestCase
{
    private static $connection = null;

    final public static function getConnection()
    {
        if (self::$connection === null) {
            self::$connection = new Connection(
                $GLOBALS['DB1_DSN'], $GLOBALS['DB1_USER'], $GLOBALS['DB1_PASSWD']
            );
        }

        return self::$connection;
    }
}