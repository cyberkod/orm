<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Exception;

class InvalidFieldType extends \InvalidArgumentException
{
}