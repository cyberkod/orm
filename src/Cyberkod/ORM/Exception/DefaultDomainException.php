<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Exception;

class DefaultDomainException extends \DomainException
{
}