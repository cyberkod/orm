<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser;

use Cyberkod\ORM\Parser\Field;
use Cyberkod\ORM\Parser\Field\Integer as FieldInteger;
use Cyberkod\ORM\Parser\Field\DynamicStr as FieldDynamicStr;
use Cyberkod\ORM\Parser\Field\StaticStr as FieldStaticStr;
use Cyberkod\ORM\Parser\Field\Date as FieldDate;

class FieldFactory
{
    const FIELD_TYPE_PATTERN = '/^(\w+)(.*)$/';

    private static $typeClasses = array(

        'tinyint' => FieldInteger::class,
        'smallint' => FieldInteger::class,
        'mediumint' => FieldInteger::class,
        'int' => FieldInteger::class,
        'bigint' => FieldInteger::class,

        'char' => FieldDynamicStr::class,
        'varchar' => FieldDynamicStr::class,

        'tinytext' => FieldStaticStr::class,
        'text' => FieldStaticStr::class,
        'mediumtext' => FieldStaticStr::class,
        'longtext' => FieldStaticStr::class,

        'datetime' => FieldDate::class, 
        'datetime2' => FieldDate::class,
        'smalldatetime' => FieldDate::class,
        'date' => FieldDate::class,
        'time' => FieldDate::class,
        'datetimeoffset' => FieldDate::class,
        'timestamp' => FieldDate::class,
    );

    public function create(string $name, string $type): Field
    {
        $matches = array();
        if (!preg_match(self::FIELD_TYPE_PATTERN, $type, $matches)) {
            throw new \InvalidArgumentException('');
        }

        $rawType = $matches[1];

        if (!isset(self::$typeClasses[$rawType])) {
            throw new \LogicException('undefined table field type');
        }
        
        $class = self::$typeClasses[$rawType];
        $reflectionMethod = new \ReflectionMethod($class, 'create');
        $field = $reflectionMethod->invoke(null, $name, $type);

        if (!$field instanceof $class) {
            throw new \LogicException('method "create" should return instance');
        }

        return $field;
    }
}