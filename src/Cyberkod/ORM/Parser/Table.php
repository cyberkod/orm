<?php

namespace Cyberkod\ORM\Parser;

use Cyberkod\ORM\Parser\Key;
use Cyberkod\ORM\Parser\Field;

class Table
{
    public $name;
    public $charset = 'utf8';
    public $collate = 'utf8_general_ci';
    public $engine = 'MyISAM';
    public $comment;

    protected $fields = array();
    protected $keys = array();

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addField(Field $field)
    {
        $name = $field->name;
        if (isset($this->fields[$name])) {
            throw new \LogicException("field named ($name) already exists in table");
        }
        $this->fields[$name] = $field;
    }

    public function addKey(Key $key)
    {
        $name = $key->getName();
        if (isset($this->keys[$name])) {
            throw new \LogicException("key named ($name) already exists in table");
        }
        $this->keys[$name] = $key;
    }

    public function getCreationSyntax()
    {
        $parts = array();

        foreach ($this->fields as $field) {
            $parts[] = $field->getCreationSyntax();
        }

        foreach ($this->keys as $key) {
            $parts[] = $key->getCreationSyntax();
        }
        
        $create = "CREATE TABLE `{$this->name}` (".PHP_EOL;
        $create .= implode(', '.PHP_EOL, $parts);
        $create .= PHP_EOL.")";
        $create .= " ENGINE={$this->engine}";
        $create .= " DEFAULT CHARSET={$this->charset}";
        $create .= " COLLATE={$this->collate}";
        $create .= empty($this->comment) ? '': " COMMENT={$this->comment}";
        $create .= ";";

        return $create;
    }

    public function getKeys()
    {
        return $this->keys;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getFieldByName(string $name)
    {
        return $this->fields[$name] ?? null;
    }
}