<?php

namespace Cyberkod\ORM\Parser\Key;

use  Cyberkod\ORM\Parser\Key as BaseKey;

class FullText extends BaseKey
{
    public function getCreationSyntax()
    {
        return 'FULLTEXT '.parent::getCreationSyntax();
    }
}