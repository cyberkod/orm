<?php

namespace Cyberkod\ORM\Parser\Key;

use  Cyberkod\ORM\Parser\Key as BaseKey;

class Unique extends BaseKey
{
    public function getCreationSyntax()
    {
        return 'UNIQUE '.parent::getCreationSyntax();
    }
}