<?php

namespace Cyberkod\ORM\Parser\Key;

use  Cyberkod\ORM\Parser\Field;
use  Cyberkod\ORM\Parser\Key as BaseKey;

class Primary extends BaseKey
{
    public function __construct(Field $field)
    {
        parent::__construct($field, 'PRIMARY');
    }

    public function getCreationSyntax()
    {
        $buffer = "PRIMARY KEY (`{$this->field->name}`)";
        return $buffer;
    }
}