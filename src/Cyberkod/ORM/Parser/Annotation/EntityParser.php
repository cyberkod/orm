<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser\Annotation;

use Cyberkod\ORM\Parser\Table;
use Cyberkod\ORM\Parser\Field;
use Cyberkod\ORM\Parser\Parsable;
use Cyberkod\ORM\Parser\KeyFactory;
use Cyberkod\ORM\Parser\FieldFactory;
use Cyberkod\Annotations\Collection;

class EntityParser implements Parsable
{
    private $table;
    private $fieldFactory;
    private $defaultProperties;

    public function __construct(\ReflectionClass $reflectionClass)
    {
        $tableName = $this->parseTableName($reflectionClass);

        $this->table = new Table($tableName);
        $this->fieldFactory = new FieldFactory();
        $this->defaultProperties = $reflectionClass->getDefaultProperties();

        $this->parseTable($reflectionClass);
        $this->parseFields($reflectionClass);
    }

    protected function parseTable(\ReflectionClass $reflectionClass)
    {
        $annotations = Collection::createFromReflectionClass($reflectionClass);

        $annotations['engine'] ?
            $this->table->engine = $annotations['engine'] : null;
        $annotations['charset'] ? 
            $this->table->charset = $annotations['charset'] : null;
        $annotations['collate'] ? 
            $this->table->collate = $annotations['collate'] : null;
        $annotations['comment'] ? 
            $this->table->comment = $annotations['comment'] : null;
    }

    protected function parseTableName(\ReflectionClass $reflectionClass)
    {
        return strtolower($reflectionClass->getShortName());
    }

    protected function parseFields(\ReflectionClass $reflectionClass)
    {
        $properties = $reflectionClass->getProperties();
        foreach ($properties as $property) {
            $field = $this->parseField($property);
            if ($field) {
                $this->table->addField($field);
            }
        }
    }

    protected function parseField(\ReflectionProperty $reflectionProperty)
    {
        $annotations = Collection::createFromReflectionProperty($reflectionProperty);

        if (!isset($annotations['type'])) {
            return null;
        }

        $name = $reflectionProperty->getName();
        $field = $this->fieldFactory->create($name, $annotations['type']);

        $annotations['collate'] ?
            $field->collate = $annotations['collate'] : null;
        $annotations['auto_increment'] ?
            $field->auto_increment = $annotations['auto_increment'] : null;
        $annotations['null'] ?
            $field->null = $annotations['null'] : null;
        $annotations['comment'] ?
            $field->comment = $annotations['comment'] : null;

        $this->parseKey($field, $annotations);

        $field->default = $this->defaultProperties[$field->name];

        return $field;
    }

    protected function parseKey(Field $field, $fieldAnnotation)
    {
        $keyFactory = new KeyFactory($field);
        $key = $keyFactory->tryCreate($fieldAnnotation);
        if ($key) {
            $field->setKey($key);
            $this->table->addKey($key);
        }
    }

    public function getTable()
    {
        return $this->table;
    }
}