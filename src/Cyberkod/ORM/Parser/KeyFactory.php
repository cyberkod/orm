<?php

namespace Cyberkod\ORM\Parser;

use ArrayAccess;
use Cyberkod\ORM\Parser\Field;
use Cyberkod\ORM\Parser\Key;

class KeyFactory
{
    private $field;
    private static $typeClasses = array(
        'primary' => Key\Primary::class,
        'key' => Key\Index::class,
        'unique' => Key\Unique::class,
        'fulltext' => Key\FullText::class,
    );

    public function __construct(Field $field)
    {
        $this->field = $field;
    }

    public function tryCreate(ArrayAccess $annotation)
    {
        foreach (self::$typeClasses as $name => $class) {
            if (isset($annotation[$name])) {
                $name = is_string($annotation[$name]) ? $annotation[$name] : '';
                return new $class($this->field, $name);
            }
        }
        return null;
    }
}