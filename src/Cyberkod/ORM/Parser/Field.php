<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser;

use Cyberkod\ORM\Parser\Key;
use Cyberkod\ORM\Exception\DefaultDomainException;

abstract class Field
{
    public $name;
    public $type;
    public $null = false;
    public $length = 0;
    public $collate = '';
    public $comment = '';
    public $auto_increment = false;
    private $key;

    protected function __construct(string $name, string $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    public function getCreationSyntax()
    {
        $type = $this->getSpecifiedCreactionSyntax();
        $buffer = "`$this->name` $type";
        $buffer .= ($this->null === true) ? '' : ' NOT NULL';
        $buffer .= ($this->auto_increment === true) ? ' AUTO_INCREMENT' : '';
        if ( ($this->null === true and $this->default === null)
            or ($this->default !== null) ) {
            $buffer .= ' DEFAULT '.$this->getDefaultSyntax();
        }
        $buffer .= (strlen($this->comment) > 0) ? " COMMENT '$this->comment'" : '';

        return $buffer;
    }

    protected function getDefaultSyntax()
    {
        $default = $this->default;
        switch (gettype($default)) {
            case 'NULL': return 'NULL';
            case 'boolean': return boolval($default);
            case 'string': return "'$default'";
            case 'integer': return "$default";
            case 'double': return "$default";
            default: 
                throw new DefaultDomainException('invalid default type');
        }
    }

    public function setKey(Key $key)
    {
        $this->key = $key;
    }

    public function getKey()
    {
        return $this->key;
    }
    
    abstract public function getSpecifiedCreactionSyntax(): string;
}