<?php

namespace Cyberkod\ORM\Parser;

interface Parsable
{
    public function getTable();
}