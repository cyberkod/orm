<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser\Field;

use Cyberkod\ORM\Parser\Field as BaseField;
use Cyberkod\ORM\Exception\InvalidFieldType;

abstract class Str extends BaseField
{
    const FIELD_TYPE_PATTERN = '/^(\w+)$/';

    public static function create(string $name, string $type)
    {
        $matches = array();
        if (!preg_match(self::FIELD_TYPE_PATTERN, $type, $matches)) {
            throw new InvalidFieldType("($type) is invalid");
        }

        $rawType = strtolower($matches[1]);
        if (!in_array($rawType, static::$allowedTypes)) {
            throw new InvalidFieldType("($rawType) is not supported by Field\\Single");
        }

        $field = new static($name, $rawType);

        return $field;
    }
    
    public function getSpecifiedCreactionSyntax(): string
    {
        return $this->type;
    }
}