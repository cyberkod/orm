<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser\Field;

use Cyberkod\ORM\Parser\Field\Str;
use Cyberkod\ORM\Exception\InvalidFieldType;

class Date extends Str
{
    protected static $allowedTypes = array(
        'datetime', 'datetime2', 'smalldatetime',
        'date', 'time', 'datetimeoffset', 'timestamp'
    );
}