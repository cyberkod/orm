<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser\Field;

use Cyberkod\ORM\Parser\Field as BaseField;
use Cyberkod\ORM\Exception\InvalidFieldType;

class Integer extends BaseField
{
    const FIELD_TYPE_PATTERN = '/^(\w+)\s*(\((\d+)\))?\s?([A-z]*)$/';
    protected static $allowedTypes = array(
        'tinyint', 'smallint', 'mediumint', 'int', 'bigint'
    );

    public static function create(string $name, string $type): self
    {
        $matches = array();
        if (!preg_match(self::FIELD_TYPE_PATTERN, $type, $matches)) {
            throw new InvalidFieldType("($type) is invalid");
        }
        
        $rawType = strtolower($matches[1]);
        if (!in_array($rawType, self::$allowedTypes)) {
            throw new InvalidFieldType("($rawType) is not supported by Field\\Integer");
        }

        $modifier = strtolower($matches[4]);
        $field = new static($name, $rawType);
        $field->length = empty($matches[3]) ? 0 : intval($matches[3]);
        $field->unsigned = (strpos($modifier, 'unsigned') !== false);

        return $field;
    }

    public function getSpecifiedCreactionSyntax(): string
    {
        $buffer = "$this->type";
        $buffer .= $this->length !== 0 ? "($this->length)" : '';
        $buffer .= $this->unsigned === true ? " unsigned" : '';

        return $buffer;
    }
}