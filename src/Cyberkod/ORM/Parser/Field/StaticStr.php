<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser\Field;

use Cyberkod\ORM\Parser\Field\Str;
use Cyberkod\ORM\Exception\InvalidFieldType;

class StaticStr extends Str
{
    protected static $allowedTypes = array(
        'tinytext', 'text', 'mediumtext', 'longtext'
    );
}