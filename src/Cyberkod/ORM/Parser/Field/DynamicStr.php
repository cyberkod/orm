<?php
declare(strict_types = 1);

namespace Cyberkod\ORM\Parser\Field;

use Cyberkod\ORM\Parser\Field\Str;
use Cyberkod\ORM\Exception\InvalidFieldType;

class DynamicStr extends Str
{
    const FIELD_TYPE_PATTERN = '/^(\w+)\s*\((\d+)\)$/';
    protected static $allowedTypes = array(
        'char', 'varchar'
    );

    public static function create(string $name, string $type): DynamicStr
    {
        $matches = array();
        if (!preg_match(self::FIELD_TYPE_PATTERN, $type, $matches)) {
            throw new InvalidFieldType("($type) is invalid");
        }

        $rawType = strtolower($matches[1]);
        if (!in_array($rawType, self::$allowedTypes)) {
            throw new InvalidFieldType("($rawType) is not supported by Field\\DynamicStr");
        }

        $field = new static($name, $rawType);
        $field->length = empty($matches[2]) ? 0 : intval($matches[2]);

        return $field;
    }

    public function getSpecifiedCreactionSyntax(): string
    {
        $buffer = "$this->type($this->length)";
        $buffer .= empty($this->collate) ? '' : " COLLATE $this->collate";

        return $buffer;
    }
}