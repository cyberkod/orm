<?php

namespace Cyberkod\ORM\Parser;

use  Cyberkod\ORM\Parser\Field;

abstract class Key
{
    protected $name;
    protected $field;

    public function __construct(Field $field, string $name)
    {
        $this->name = empty($name) ? strtolower($field->name) : $name;
        $this->field = $field;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCreationSyntax()
    {
        $buffer = "KEY `{$this->name}` (`{$this->field->name}`)";
        return $buffer;
    }
}