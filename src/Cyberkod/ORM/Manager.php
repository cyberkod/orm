<?php

namespace Cyberkod\ORM;

use Cyberkod\ORM\Selector;
use Cyberkod\ORM\Connection;
use Cyberkod\ORM\EntityProjection;
use Cyberkod\ORM\Exception\EntityNotExistsException;

class Manager
{
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function registerEntity(string $entityName, string $entityClass): self
    {
        $this->registredEntityProjections[$entityName] = $entityClass;
        return $this;
    }

    public function createEntity(string $entityName)
    {
        $entityProjection = $this->getEntityProjection($entityName);
        return $entityProjection->createEntity(array($this));
    }

    public function createSelector(string $entityName)
    {
        $entityProjection = $this->getEntityProjection($entityName);
        return new Selector($this, $entityClass);
    }

    public function getEntityProjection(string $entityName)
    {
        if (!isset($this->entityProjections[$entityName])) {

            if (!isset($this->registredEntityProjections[$entityName])) {
                throw new EntityNotExistsException("entity named ($entityName) not exists");
            }

            $entityClass = $this->registredEntityProjections[$entityName];
            $projection = new EntityProjection($entityName, $entityClass);
            $this->entityProjections[$entityName] = $projection;
        }

        return $this->entityProjections[$entityName];
    }

    public function getConnection(): Connection
    {
        return $this->connection;
    }
}