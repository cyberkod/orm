<?php

namespace Cyberkod\ORM;

use Cyberkod\ORM\Entity;
use Cyberkod\ORM\Manager;

class Selector
{
    private $manager;
    private $entityClass;

    public function __construct(Manager $manager, $entityClass)
    {
        $this->manager = $manager;
        $this->entityClass = $entityClass;
    }
    
    public function getByPrimaryID(int $primaryID)
    {
        $sql = "SELECT * FROM user WHERE userID = :userID LIMIT 1";

        $connection = $this->manager->getConnection();
        $statement = $connection->prepare($sql);
        $statement->bindValue(':userID', $primaryID, $connection::PARAM_INT);
        $statement->execute();

        $args = array($this->manager);
        $entity = $statement->fetchObject($this->entityClass, $args);
        $statement->closeCursor();

        return $entity;
    }
}