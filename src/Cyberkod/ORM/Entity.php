<?php

namespace Cyberkod\ORM;

use Cyberkod\ORM\Manager;

class Entity
{
    private $manager;
    private $primary = 0;
    private $tableName;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;        
    }

    public function getPrimary(): int
    {
        return $this->primary;
    }

    public function isPreserve(): bool
    {
        return (bool)$this->primary;
    }

    public function save(): bool
    {
        



        $storage = $this->manager->getConnection();
        $primary = $storage->persist($this);
        //$this->primary = $primary;

        return true;
    }
}