<?php

namespace Cyberkod\ORM;

use PDO;
use Cyberkod\ORM\Entity;

class Connection extends PDO
{
    public function __construct($dsn, $username, $password)
    {
        $options = array(parent::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'");
        parent::__construct($dsn, $username, $password, $options);
        $this->setAttribute(parent::ATTR_ERRMODE, parent::ERRMODE_EXCEPTION);
    }

    public function query($query)
    {
        $this->log($query);
        $args = array($this->bindTableName($query));
        return call_user_func_array(array('parent', __FUNCTION__), $args);
    }

    public function prepare($query, $options = array())
    {
        $this->log($query);
        $args = array($this->bindTableName($query), $options);
        return call_user_func_array(array('parent', __FUNCTION__), $args);
    }

    public function exec($query)
    {
        $this->log($query);
        $args = array($this->bindTableName($query));
        return call_user_func_array(array('parent', __FUNCTION__), $args);
    }

    public function log($query)
    {
        print_r($query);
    }

    public function bindTableName($query)
    {
        return $query;
    }

    public function getSingleFromQueryAsClass($query, string $className)
    {
        
    }

    public function persist(Entity $entity): bool
    {
        return true;
    }
}