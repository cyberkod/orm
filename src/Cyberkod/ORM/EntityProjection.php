<?php

namespace Cyberkod\ORM;

use Cyberkod\ORM\Parser\Annotation\EntityParser;

class EntityProjection
{
    private $name;
    private $mappedClass;

    public function __construct(string $entityName, string $entityMappedClass)
    {
        /*if (!class_exists($className)) {
            throw new EntityNotExistsException("entity named ($entityName) not exists");
        }*/
        $this->name = $entityName;
        $this->mappedClass = $entityMappedClass;
        $this->reflectionEntityClass = new \ReflectionClass($this->mappedClass);
        $parser = new EntityParser($this->reflectionEntityClass);
        $this->table = $parser->getTable();
    }

    public function createEntity(array $arguments = array())
    {
        return $this->reflectionEntityClass->newInstanceArgs($arguments);
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getName()
    {
        return $this->name;
    }
}